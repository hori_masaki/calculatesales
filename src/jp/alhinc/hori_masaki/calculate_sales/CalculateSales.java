package jp.alhinc.hori_masaki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		// 支店コード読み取り
		Map<String, Long> amounts = new HashMap<>();
		Map<String, String> names = new HashMap<>();

		try {
            File branchLst = new File(args[0], "branch.lst");

			if(!branchLst.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
            }

		    String line;
		    BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(branchLst));
				while ((line = br.readLine()) != null) {
					String[] items = line.split(",");
					if (items.length != 2 || !items[0].matches("\\d{3}") || items[1].isEmpty()) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					names.put(items[0], items[1]);
					amounts.put(items[0], 0L);
				}
            }
			finally {
				br.close();
			}

			// 売上読み取り
            File directory = new File(args[0]);

            List<File> rcdFiles = new ArrayList<>();
            for (File file : directory.listFiles()) {
                if (file.getName().matches("\\d{8}.rcd")) {
                    rcdFiles.add(file);
                }
            }

            Collections.sort(rcdFiles);
            int min = Integer.parseInt(rcdFiles.get(0).getName().substring(0, 8));
            int max = Integer.parseInt(rcdFiles.get(rcdFiles.size() - 1).getName().substring(0, 8));

            if (max != min + rcdFiles.size() - 1) {
                System.out.println("売上ファイル名が連番になっていません");
                return;
            }

            for (File rcdFile : rcdFiles) {
                try {
                    br = new BufferedReader(new FileReader(rcdFile));
                    String code = br.readLine();
                    Long amount = Long.parseLong(br.readLine());
                    if(br.readLine() != null) {
                        System.out.println(rcdFile.getName() + "のフォーマットが不正です");
                        return;
                    }

                    if (!names.containsKey(code)) {
                        System.out.println(rcdFile.getName() + "の支店コードが不正です");
                        return;
                    }

                    Long sum = amount + amounts.get(code);
                    if (sum.toString().length() > 10) {
                        System.out.println("合計金額が10桁を超えました");
                        return;
                    }
                    amounts.put(code, sum);
                } finally {
                    br.close();
                }
            }

            // 集計結果出力
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));
			for (String key : names.keySet()) {
				pw.println(key + "," + names.get(key) + "," + amounts.get(key));
			}
			pw.close();
		}catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
    }
}

